/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ainunfajar
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
public class DataDiri {


    //tambahkan nama
    public static String nama99 = "MH. Ainun Fajar";
    public static String nama12 = "Nadiatu Wafrina";

    public static String nama08 = "M.Alfan Nashrullah";

    public static String nama58 = "nur diana fahma salsabila";
    public static String nama38 = "Agus Stiawan";
    public static String nama42 = "Arya Bimantoro";
    public static String nama47 = "Ika Nuril";
    public static String nama17= "Harun Bahahruddin";
    public static String nama20= "Aulia Cahya Rani";
    public static String nama50 ="Masfufahtul Umroh"; 

    public static String nama33 = "Syafrina Dyah Kusuma Wardani";
    public static String nama40 = "Amalia Salsabilla Ariyanto";
    public static String nama59 = "Nur Diana Fahma Salsabila";
    public static String nama22 = "Ifatus Sufairoh";
    public static String nama56 = "Nabilatur Rahma";
    public static String nama29 = "Novita Khasanah";

    public static String getNama08() {
        return nama08;
    }

    public static String getNama47() {
        return nama47;
    }

    public static String getNama38() {
        return nama38;
    }

    public static String getNama42() {
        return nama42;
    }

	// public static String getNama99() {
 public static String getNama59() {
        return nama59;
    }

    public static String getNama99() {
        return nama99;
    }
     public static String getNama12() {
        return nama12;
    }

    public static String getNama40(){
        return nama40;
    }

    public static String getNama58() {
        return nama58;
    }

    //untuk memberi nilai atau isi pada variabel nama03
    public static String nama03 = "Indah Kusumawati";

    public static String getNama03() {
        return nama03;
    }

    //untuk mendeklarasikan dan merumuskan penentuan umur

    public static String getNama20() {
        return nama20;
    }

    public static String getNama50() {
        return nama50;
    }

    public static String getNama17() {
        return nama17;
    }

    public static String getNama33() {
        return nama33;
    }
    public static String getNama22() {
    	return nama22;
    }
    public static String getNama56() {
        return nama56;
    }
    public static String getNama29() {
    return nama29;
    }

    public static int hitungUmur(int lahir) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy");
	    Date date = new Date();
        String tahun = dateFormat.format(date);
        int tahunSekarang = Integer.parseInt(tahun);
        int umur = tahunSekarang - lahir;
        return umur;
    }
    //untuk mendeklarasikan penentuan nim
    public static void getNim(String absen, String jalur, String angkatan){
        String kodejalur = "";
        if(jalur == "mandiri"){
            kodejalur = "0";
        }if(jalur == "tes"){
            kodejalur = "9";
        }if(jalur == "undangan"){
            kodejalur = "7";
        }
        System.out.println("Nim saya : H"+kodejalur+"62"+angkatan+"0"+absen);
    }


    public static void main(String[] args) {
        String namaSaya58 = getNama58();//ambil data nama
        System.out.println("nama" +namaSaya58);

        System.out.println("\nNama saya: " + getNama08());
        int umurAku = hitungUmur(2001);
        System.out.println("Umur Saya : " + umurAku);
        getNim("08","mandiri","19");

        // mahasiswa38
        System.out.println("\nNama saya: " + getNama38());
        System.out.println("Umur saya: " + hitungUmur(2000));
        getNim("38","tes","19");
        System.out.println();


        //mahasiswa03
        System.out.println("Nama saya: " +getNama03());
        int umurSaya = hitungUmur(2001);//masukkan tahun lahir
        System.out.println("Umur saya: " + umurSaya);
        getNim("03","mandiri","19");//masukkan absen(2 angka terakhir dari nim), jalur masuk, angakatan ke


        //mahasiswa 50
        System.out.println();
        String nama50 = getNama50();
        System.out.println("nama saya : " + nama50);
        int umur50 = hitungUmur(2001);
        System.out.println("umur saya : " + umur50);
        getNim("50","tes","19");


        //mahasiswa 22
        System.out.println();     
        String nama22 = getNama22();
        System.out.println("nama saya : " + nama22);
        int umur22 = hitungUmur(2001);
        System.out.println("umur saya : " + umur22);
        getNim("22","undangan","19");


        //mahasiswa 33
        System.out.println();
        String nama33 = getNama33();
        System.out.println("nama saya :" + nama33);
        int umur33 = hitungUmur(2001);
        System.out.println("umur saya : " + umur33);
        getNim("33","undangan","19");

        //mahasiswa 59
        System.out.println();
        String nama59 = getNama59();
        System.out.println("nama saya : " + nama59);
        int umur59 = hitungUmur(2001);
        System.out.println("umur saya : " + umur59);
        getNim("59","tes","19");


        //mahasiswa 40
        System.out.println();
        String nama40 = getNama40();
        System.out.println("nama saya : " + nama40);
        int umur40 = hitungUmur(2001);
        System.out.println("umur saya : " + umur40);
        getNim("40","tes","19");

        //mahasiswa 56
        System.out.println();
        String nama56 = getNama56();
        System.out.println("nama saya : " + nama56);
        int umur56 = hitungUmur(2001);
        System.out.println("umur saya : " + umur56);
        getNim("56","tes","19");

        System.out.println();     
        String namaBima42 = getNama42();
        System.out.println("nama saya : " + namaBima42);
        int umurBima42 = hitungUmur(2001);
        System.out.println("umur saya : " + umurBima42);
        getNim("42","tes","19");

        System.out.println();     
        String nama47 = getNama47();
        System.out.println("nama saya : " + nama47);
        int umur47 = hitungUmur(2000);
        System.out.println("umur saya : " + umur47);
        getNim("47","tes","19");

        System.out.println();     
        String nama17 = getNama17();
        System.out.println("nama saya : " + nama17);
        int umur17 = hitungUmur(2001);
        System.out.println("umur saya : " + umur17);
        getNim("02","mandiri","19");

        System.out.println();     
        String nama20 = getNama20();
        System.out.println("nama saya : " + nama20);
        int umur20 = hitungUmur(2000);
        System.out.println("umur saya : " + umur20);
        getNim("20","undangan","19");

        String namaku = getNama12();
        System.out.println("\nNama saya: " + namaku);
        int umurku = hitungUmur(1999);
        System.out.println("Umur saya: " + umurku);
        getNim("0","mandiri","4");

        System.out.println();     
        String nama29 = getNama29();
        System.out.println("nama saya : " + nama29);
        int umur29 = hitungUmur(2000);
        System.out.println("umur saya : " + umur29);
        getNim("29","undangan","19");
    }
}
